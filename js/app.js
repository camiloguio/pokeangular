(function () {

var app = angular.module('pokedex', []);

app.controller('PokemonController', function (){
    this.pokemon = {
        id: 001,
        name: 'Squirtle',
        type: ['Water', 'Force'],
        Height: "0.65 m",
        Atacks: ['Gun water', 'hit faster'],
        evolution: ['Wartortle', 'Blastoise', 'Megablastoise']
                
    };

    this.stats = {
        Power: '50pw',
        Velocity: '30pw',
        Stamina: '70pw' 
    };

});



app.controller('TabsController', function (){
    this.tab = 1;

    this.selecTab = function(tab){
        this.tab = tab;
    };
});

app.controller('CommentsController', function(){

    this.comments = [];
    this.comen = {};
    this.show = false;

    this.statusbtn = function(){
    this.show = !this.show;
    }

    this.anonymousChanged = function(){

        if(this.comen.anonymus){
            this.comen.email = "";
        }

    };

    this.nueComment = function(){
        this.comen.date = Date.now();
        this.comments.push(this.comen);
        this.comen = {};
     
        
    };
    
});

app.directive('pokemonData', function() {

    return{
    
        restrict: 'E',
        templateUrl: 'partials/pokemon-data.html'
    
    };

});

app.directive('pokemonStats', function(){

    return{
        restrict: 'E',
        templateUrl: 'partials/pokemon-stats.html'
    }

})

app.directive('pokemonEvolution', function(){

    return{
        restrict: 'E',
        templateUrl: 'partials/pokemon-evolution.html'
    }

})

app.directive('pokemonComment', function(){

    return{
        restrict: 'E',
        templateUrl: 'partials/pokemon-comment.html'
    }

})

app.filter('imagefy', function (){

return function (input){
    var url = "img/" + input.toLowerCase() + ".jpg";
    return url; 
}

});



})();